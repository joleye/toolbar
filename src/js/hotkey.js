/**
 * Copyright (c) 2023. joleye.com all rights reserved..
 * 大屏模拟方向键 0.0.1
 * @anther joleye
 * https://github.com/joleye/toolbar
 */

define(['jquery'], function (jQuery) {
    var right = function () {
        //1、当向右时，获取所有left大于触发焦点的元素left的元素
        //2、在获取到的元素集合中取top值最接近触发焦点元素top的元素
        //3、设置得到元素的焦点
        var buts = [];
        var top = Number(selectBut.offsetTop); //获取当前元素的top
        var left = Number(selectBut.offsetLeft); //获取当前元素的left
        for (var i = 0; i < _Btns.length; i++) {
            var but1 = _Btns[i];
            if (selectBut !== but1) {
                var _left = Number(but1.offsetLeft);
                //  var _top = Number(but1.offsetTop);
                if (_left > left) {
                    buts.push(but1);
                }
            }
        }
        if (buts && buts.length > 0) {
            selectBut = buts.sort(function (a, b) { //获取top最接近触发事件元素的元素
                return Math.abs(a.offsetTop - top) - Math.abs(b.offsetTop - top);
            })[0];
            selectBut.focus();
        }
    };

    var left = function () {
        //1、当向左时，获取所有left小于触发焦点的元素left的元素
        //2、在获取到的元素集合中取top值最接近触发焦点元素top的元素
        //3、设置得到元素的焦点
        var buts = [];
        var top = Number(selectBut.offsetTop); //获取当前元素的top
        var left = Number(selectBut.offsetLeft); //获取当前元素的left
        for (var i = 0; i < _Btns.length; i++) {
            var but1 = _Btns[i];
            if (selectBut !== but1) {
                var _left = Number(but1.offsetLeft);
                if (_left < left) {
                    buts.push(but1);
                }
            }
        }
        if (buts && buts.length > 0) {
            selectBut = buts.sort(function (a, b) { //获取top最接近触发事件元素的元素
                return Math.abs(a.offsetLeft - left) - Math.abs(b.offsetLeft - left);
            })[0];
            selectBut.focus();
        }
    };

    var down = function () {
        //1、当向下时，获取所有top大于触发焦点的元素top的元素
        //2、在获取到的元素集合中取left值最接近触发焦点元素left的元素
        //3、设置得到元素的焦点
        var buts = [];
        var top = Number(selectBut.offsetTop); //获取当前元素的top
        var left = Number(selectBut.offsetLeft); //获取当前元素的left
        for (var i = 0; i < _Btns.length; i++) {
            var but1 = _Btns[i];
            if (selectBut !== but1) {
                var _top = Number(but1.offsetTop);
                if (_top > top) {
                    buts.push(but1);
                }
            }
        }
        if (buts && buts.length > 0) {
            selectBut = buts.sort(function (a, b) { //获取top最接近触发事件元素的元素
                return Math.abs(a.offsetLeft - left) - Math.abs(b.offsetLeft - left);
            })[0];
            selectBut.focus();
        }
    };

    var up = function () {
        //1、当向上时，获取所有top小于触发焦点的元素top的元素
        //2、在获取到的元素集合中取left值最接近触发焦点元素left的元素
        //3、设置得到元素的焦点
        var buts = [];
        var top = Number(selectBut.offsetTop); //获取当前元素的top
        var left = Number(selectBut.offsetLeft); //获取当前元素的left
        for (var i = 0; i < _Btns.length; i++) {
            var but1 = _Btns[i];
            if (selectBut !== but1) {
                var _top = Number(but1.offsetTop);
                if (_top < top) {
                    buts.push(but1);
                }
            }
        }
        if (buts && buts.length > 0) {
            selectBut = buts.sort(function (a, b) { //获取top最接近触发事件元素的元素
                return Math.abs(a.offsetTop - top) - Math.abs(b.offsetTop - top);
            })[0];
            selectBut.focus();
        }
    };

    $(document).keydown(function (event) {
        console.log(event);
        if (event.keyCode === 37) {
            left();
        } else if (event.keyCode === 38) {
            up();
        } else if (event.keyCode === 39) {
            right();
        } else if (event.keyCode === 40) {
            down();
        } else if (event.keyCode === 13) {
            //确定
        } else {
            //alert(event.keyCode);
            //event.preventDefault();
            //return false;
        }
        eventCallback && eventCallback();
    });

    var _Btns = null;
    var selectBut = null;
    var eventCallback = null;

    return {
        left: left,
        right: right,
        up: up,
        down: down,
        init: function (conf) {
            if (typeof conf === 'object') {
                if (conf.btns) {
                    _Btns = conf.btns;
                } else {
                    _Btns = $('a,input');
                }
                eventCallback = conf.eventCallback;
            } else {
                _Btns = $('a,input');
                eventCallback = conf;
            }
            if (_Btns.length > 0) {
                selectBut = _Btns[0];
                selectBut.focus();
            }
        }
    };
});
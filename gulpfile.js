let gulp = require('gulp');
let less = require('gulp-less');
//当发生异常时提示错误 确保本地安装gulp-notify和gulp-plumber
let notify = require('gulp-notify');
let plumber = require('gulp-plumber');
let rename = require('gulp-rename');
let cleanCSS = require('gulp-clean-css');

gulp.task('less', function () {
    return gulp.src('src/css/*.less')
        .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
        .pipe(less())
        .pipe(gulp.dest('src/css'));
});

//压缩css
gulp.task('minifycss', function () {
    return gulp.src(['src/css/*.css', '!src/css/*.min.css'])    //需要操作的文件
        .pipe(rename({suffix: '.min'}))   //rename压缩后的文件名
        .pipe(cleanCSS({compatibility: 'ie7'}))   //执行压缩
        .pipe(gulp.dest('src/css'));   //输出文件夹
});

gulp.task('watch', function () {
    gulp.watch('src/css/*.less', gulp.series('less', 'minifycss'));
});

gulp.task('build', gulp.series('less', 'minifycss'));

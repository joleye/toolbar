require.config({
    baseUrl: './js/',
    paths: {
        'jquery': './jquery-1.11.0.min',
        'processLoading': '../../src/js/processLoading',
        'upload': '../../src/js/jquery.upload',
        'toolbar': '../../src/js/jquery.toolbar',
    },
    shim: {
        'processLoading': ['jquery']
    },
});